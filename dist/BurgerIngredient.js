"use strict";

function _typeof51(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof51 = function _typeof51(obj) { return typeof obj; }; } else { _typeof51 = function _typeof51(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof51(obj); }

function _typeof50(obj) {
  if (typeof Symbol === "function" && _typeof51(Symbol.iterator) === "symbol") {
    _typeof50 = function _typeof50(obj) {
      return _typeof51(obj);
    };
  } else {
    _typeof50 = function _typeof50(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof51(obj);
    };
  }

  return _typeof50(obj);
}

function _typeof49(obj) {
  if (typeof Symbol === "function" && _typeof50(Symbol.iterator) === "symbol") {
    _typeof49 = function _typeof49(obj) {
      return _typeof50(obj);
    };
  } else {
    _typeof49 = function _typeof49(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof50(obj);
    };
  }

  return _typeof49(obj);
}

function _typeof48(obj) {
  if (typeof Symbol === "function" && _typeof49(Symbol.iterator) === "symbol") {
    _typeof48 = function _typeof48(obj) {
      return _typeof49(obj);
    };
  } else {
    _typeof48 = function _typeof48(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof49(obj);
    };
  }

  return _typeof48(obj);
}

function _typeof47(obj) {
  if (typeof Symbol === "function" && _typeof48(Symbol.iterator) === "symbol") {
    _typeof47 = function _typeof47(obj) {
      return _typeof48(obj);
    };
  } else {
    _typeof47 = function _typeof47(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof48(obj);
    };
  }

  return _typeof47(obj);
}

function _typeof46(obj) {
  if (typeof Symbol === "function" && _typeof47(Symbol.iterator) === "symbol") {
    _typeof46 = function _typeof46(obj) {
      return _typeof47(obj);
    };
  } else {
    _typeof46 = function _typeof46(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof47(obj);
    };
  }

  return _typeof46(obj);
}

function _typeof45(obj) {
  if (typeof Symbol === "function" && _typeof46(Symbol.iterator) === "symbol") {
    _typeof45 = function _typeof45(obj) {
      return _typeof46(obj);
    };
  } else {
    _typeof45 = function _typeof45(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof46(obj);
    };
  }

  return _typeof45(obj);
}

function _typeof44(obj) {
  if (typeof Symbol === "function" && _typeof45(Symbol.iterator) === "symbol") {
    _typeof44 = function _typeof44(obj) {
      return _typeof45(obj);
    };
  } else {
    _typeof44 = function _typeof44(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof45(obj);
    };
  }

  return _typeof44(obj);
}

function _typeof43(obj) {
  if (typeof Symbol === "function" && _typeof44(Symbol.iterator) === "symbol") {
    _typeof43 = function _typeof43(obj) {
      return _typeof44(obj);
    };
  } else {
    _typeof43 = function _typeof43(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof44(obj);
    };
  }

  return _typeof43(obj);
}

function _typeof42(obj) {
  if (typeof Symbol === "function" && _typeof43(Symbol.iterator) === "symbol") {
    _typeof42 = function _typeof42(obj) {
      return _typeof43(obj);
    };
  } else {
    _typeof42 = function _typeof42(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof43(obj);
    };
  }

  return _typeof42(obj);
}

function _typeof41(obj) {
  if (typeof Symbol === "function" && _typeof42(Symbol.iterator) === "symbol") {
    _typeof41 = function _typeof41(obj) {
      return _typeof42(obj);
    };
  } else {
    _typeof41 = function _typeof41(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof42(obj);
    };
  }

  return _typeof41(obj);
}

function _typeof40(obj) {
  if (typeof Symbol === "function" && _typeof41(Symbol.iterator) === "symbol") {
    _typeof40 = function _typeof40(obj) {
      return _typeof41(obj);
    };
  } else {
    _typeof40 = function _typeof40(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof41(obj);
    };
  }

  return _typeof40(obj);
}

function _typeof39(obj) {
  if (typeof Symbol === "function" && _typeof40(Symbol.iterator) === "symbol") {
    _typeof39 = function _typeof39(obj) {
      return _typeof40(obj);
    };
  } else {
    _typeof39 = function _typeof39(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof40(obj);
    };
  }

  return _typeof39(obj);
}

function _typeof38(obj) {
  if (typeof Symbol === "function" && _typeof39(Symbol.iterator) === "symbol") {
    _typeof38 = function _typeof38(obj) {
      return _typeof39(obj);
    };
  } else {
    _typeof38 = function _typeof38(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof39(obj);
    };
  }

  return _typeof38(obj);
}

function _typeof37(obj) {
  if (typeof Symbol === "function" && _typeof38(Symbol.iterator) === "symbol") {
    _typeof37 = function _typeof37(obj) {
      return _typeof38(obj);
    };
  } else {
    _typeof37 = function _typeof37(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof38(obj);
    };
  }

  return _typeof37(obj);
}

function _typeof36(obj) {
  if (typeof Symbol === "function" && _typeof37(Symbol.iterator) === "symbol") {
    _typeof36 = function _typeof36(obj) {
      return _typeof37(obj);
    };
  } else {
    _typeof36 = function _typeof36(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof37(obj);
    };
  }

  return _typeof36(obj);
}

function _typeof35(obj) {
  if (typeof Symbol === "function" && _typeof36(Symbol.iterator) === "symbol") {
    _typeof35 = function _typeof35(obj) {
      return _typeof36(obj);
    };
  } else {
    _typeof35 = function _typeof35(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof36(obj);
    };
  }

  return _typeof35(obj);
}

function _typeof34(obj) {
  if (typeof Symbol === "function" && _typeof35(Symbol.iterator) === "symbol") {
    _typeof34 = function _typeof34(obj) {
      return _typeof35(obj);
    };
  } else {
    _typeof34 = function _typeof34(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof35(obj);
    };
  }

  return _typeof34(obj);
}

function _typeof33(obj) {
  if (typeof Symbol === "function" && _typeof34(Symbol.iterator) === "symbol") {
    _typeof33 = function _typeof33(obj) {
      return _typeof34(obj);
    };
  } else {
    _typeof33 = function _typeof33(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof34(obj);
    };
  }

  return _typeof33(obj);
}

function _typeof32(obj) {
  if (typeof Symbol === "function" && _typeof33(Symbol.iterator) === "symbol") {
    _typeof32 = function _typeof32(obj) {
      return _typeof33(obj);
    };
  } else {
    _typeof32 = function _typeof32(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof33(obj);
    };
  }

  return _typeof32(obj);
}

function _typeof31(obj) {
  if (typeof Symbol === "function" && _typeof32(Symbol.iterator) === "symbol") {
    _typeof31 = function _typeof31(obj) {
      return _typeof32(obj);
    };
  } else {
    _typeof31 = function _typeof31(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof32(obj);
    };
  }

  return _typeof31(obj);
}

function _typeof30(obj) {
  if (typeof Symbol === "function" && _typeof31(Symbol.iterator) === "symbol") {
    _typeof30 = function _typeof30(obj) {
      return _typeof31(obj);
    };
  } else {
    _typeof30 = function _typeof30(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof31(obj);
    };
  }

  return _typeof30(obj);
}

function _typeof29(obj) {
  if (typeof Symbol === "function" && _typeof30(Symbol.iterator) === "symbol") {
    _typeof29 = function _typeof29(obj) {
      return _typeof30(obj);
    };
  } else {
    _typeof29 = function _typeof29(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof30(obj);
    };
  }

  return _typeof29(obj);
}

function _typeof28(obj) {
  if (typeof Symbol === "function" && _typeof29(Symbol.iterator) === "symbol") {
    _typeof28 = function _typeof28(obj) {
      return _typeof29(obj);
    };
  } else {
    _typeof28 = function _typeof28(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof29(obj);
    };
  }

  return _typeof28(obj);
}

function _typeof27(obj) {
  if (typeof Symbol === "function" && _typeof28(Symbol.iterator) === "symbol") {
    _typeof27 = function _typeof27(obj) {
      return _typeof28(obj);
    };
  } else {
    _typeof27 = function _typeof27(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof28(obj);
    };
  }

  return _typeof27(obj);
}

function _typeof26(obj) {
  if (typeof Symbol === "function" && _typeof27(Symbol.iterator) === "symbol") {
    _typeof26 = function _typeof26(obj) {
      return _typeof27(obj);
    };
  } else {
    _typeof26 = function _typeof26(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof27(obj);
    };
  }

  return _typeof26(obj);
}

function _typeof25(obj) {
  if (typeof Symbol === "function" && _typeof26(Symbol.iterator) === "symbol") {
    _typeof25 = function _typeof25(obj) {
      return _typeof26(obj);
    };
  } else {
    _typeof25 = function _typeof25(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof26(obj);
    };
  }

  return _typeof25(obj);
}

function _typeof24(obj) {
  if (typeof Symbol === "function" && _typeof25(Symbol.iterator) === "symbol") {
    _typeof24 = function _typeof24(obj) {
      return _typeof25(obj);
    };
  } else {
    _typeof24 = function _typeof24(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof25(obj);
    };
  }

  return _typeof24(obj);
}

function _typeof23(obj) {
  if (typeof Symbol === "function" && _typeof24(Symbol.iterator) === "symbol") {
    _typeof23 = function _typeof23(obj) {
      return _typeof24(obj);
    };
  } else {
    _typeof23 = function _typeof23(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof24(obj);
    };
  }

  return _typeof23(obj);
}

function _typeof22(obj) {
  if (typeof Symbol === "function" && _typeof23(Symbol.iterator) === "symbol") {
    _typeof22 = function _typeof22(obj) {
      return _typeof23(obj);
    };
  } else {
    _typeof22 = function _typeof22(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof23(obj);
    };
  }

  return _typeof22(obj);
}

function _typeof21(obj) {
  if (typeof Symbol === "function" && _typeof22(Symbol.iterator) === "symbol") {
    _typeof21 = function _typeof21(obj) {
      return _typeof22(obj);
    };
  } else {
    _typeof21 = function _typeof21(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof22(obj);
    };
  }

  return _typeof21(obj);
}

function _typeof20(obj) {
  if (typeof Symbol === "function" && _typeof21(Symbol.iterator) === "symbol") {
    _typeof20 = function _typeof20(obj) {
      return _typeof21(obj);
    };
  } else {
    _typeof20 = function _typeof20(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof21(obj);
    };
  }

  return _typeof20(obj);
}

function _typeof19(obj) {
  if (typeof Symbol === "function" && _typeof20(Symbol.iterator) === "symbol") {
    _typeof19 = function _typeof19(obj) {
      return _typeof20(obj);
    };
  } else {
    _typeof19 = function _typeof19(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof20(obj);
    };
  }

  return _typeof19(obj);
}

function _typeof18(obj) {
  if (typeof Symbol === "function" && _typeof19(Symbol.iterator) === "symbol") {
    _typeof18 = function _typeof18(obj) {
      return _typeof19(obj);
    };
  } else {
    _typeof18 = function _typeof18(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof19(obj);
    };
  }

  return _typeof18(obj);
}

function _typeof17(obj) {
  if (typeof Symbol === "function" && _typeof18(Symbol.iterator) === "symbol") {
    _typeof17 = function _typeof17(obj) {
      return _typeof18(obj);
    };
  } else {
    _typeof17 = function _typeof17(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof18(obj);
    };
  }

  return _typeof17(obj);
}

function _typeof16(obj) {
  if (typeof Symbol === "function" && _typeof17(Symbol.iterator) === "symbol") {
    _typeof16 = function _typeof16(obj) {
      return _typeof17(obj);
    };
  } else {
    _typeof16 = function _typeof16(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof17(obj);
    };
  }

  return _typeof16(obj);
}

function _typeof15(obj) {
  if (typeof Symbol === "function" && _typeof16(Symbol.iterator) === "symbol") {
    _typeof15 = function _typeof15(obj) {
      return _typeof16(obj);
    };
  } else {
    _typeof15 = function _typeof15(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof16(obj);
    };
  }

  return _typeof15(obj);
}

function _typeof14(obj) {
  if (typeof Symbol === "function" && _typeof15(Symbol.iterator) === "symbol") {
    _typeof14 = function _typeof14(obj) {
      return _typeof15(obj);
    };
  } else {
    _typeof14 = function _typeof14(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof15(obj);
    };
  }

  return _typeof14(obj);
}

function _typeof13(obj) {
  if (typeof Symbol === "function" && _typeof14(Symbol.iterator) === "symbol") {
    _typeof13 = function _typeof13(obj) {
      return _typeof14(obj);
    };
  } else {
    _typeof13 = function _typeof13(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof14(obj);
    };
  }

  return _typeof13(obj);
}

function _typeof12(obj) {
  if (typeof Symbol === "function" && _typeof13(Symbol.iterator) === "symbol") {
    _typeof12 = function _typeof12(obj) {
      return _typeof13(obj);
    };
  } else {
    _typeof12 = function _typeof12(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof13(obj);
    };
  }

  return _typeof12(obj);
}

function _typeof11(obj) {
  if (typeof Symbol === "function" && _typeof12(Symbol.iterator) === "symbol") {
    _typeof11 = function _typeof11(obj) {
      return _typeof12(obj);
    };
  } else {
    _typeof11 = function _typeof11(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof12(obj);
    };
  }

  return _typeof11(obj);
}

function _typeof10(obj) {
  if (typeof Symbol === "function" && _typeof11(Symbol.iterator) === "symbol") {
    _typeof10 = function _typeof10(obj) {
      return _typeof11(obj);
    };
  } else {
    _typeof10 = function _typeof10(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof11(obj);
    };
  }

  return _typeof10(obj);
}

function _typeof9(obj) {
  if (typeof Symbol === "function" && _typeof10(Symbol.iterator) === "symbol") {
    _typeof9 = function _typeof9(obj) {
      return _typeof10(obj);
    };
  } else {
    _typeof9 = function _typeof9(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof10(obj);
    };
  }

  return _typeof9(obj);
}

function _typeof8(obj) {
  if (typeof Symbol === "function" && _typeof9(Symbol.iterator) === "symbol") {
    _typeof8 = function _typeof8(obj) {
      return _typeof9(obj);
    };
  } else {
    _typeof8 = function _typeof8(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof9(obj);
    };
  }

  return _typeof8(obj);
}

function _typeof7(obj) {
  if (typeof Symbol === "function" && _typeof8(Symbol.iterator) === "symbol") {
    _typeof7 = function _typeof7(obj) {
      return _typeof8(obj);
    };
  } else {
    _typeof7 = function _typeof7(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof8(obj);
    };
  }

  return _typeof7(obj);
}

function _typeof6(obj) {
  if (typeof Symbol === "function" && _typeof7(Symbol.iterator) === "symbol") {
    _typeof6 = function _typeof6(obj) {
      return _typeof7(obj);
    };
  } else {
    _typeof6 = function _typeof6(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof7(obj);
    };
  }

  return _typeof6(obj);
}

function _typeof5(obj) {
  if (typeof Symbol === "function" && _typeof6(Symbol.iterator) === "symbol") {
    _typeof5 = function _typeof5(obj) {
      return _typeof6(obj);
    };
  } else {
    _typeof5 = function _typeof5(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof6(obj);
    };
  }

  return _typeof5(obj);
}

function _typeof4(obj) {
  if (typeof Symbol === "function" && _typeof5(Symbol.iterator) === "symbol") {
    _typeof4 = function _typeof4(obj) {
      return _typeof5(obj);
    };
  } else {
    _typeof4 = function _typeof4(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof5(obj);
    };
  }

  return _typeof4(obj);
}

function _typeof3(obj) {
  if (typeof Symbol === "function" && _typeof4(Symbol.iterator) === "symbol") {
    _typeof3 = function _typeof3(obj) {
      return _typeof4(obj);
    };
  } else {
    _typeof3 = function _typeof3(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof4(obj);
    };
  }

  return _typeof3(obj);
}

function _typeof2(obj) {
  if (typeof Symbol === "function" && _typeof3(Symbol.iterator) === "symbol") {
    _typeof2 = function _typeof2(obj) {
      return _typeof3(obj);
    };
  } else {
    _typeof2 = function _typeof2(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof3(obj);
    };
  }

  return _typeof2(obj);
}

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _BurgerIngredient = _interopRequireDefault(require("./BurgerIngredient.css"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

function _typeof(obj) {
  if (typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol") {
    _typeof = function _typeof(obj) {
      return _typeof2(obj);
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
    };
  }

  return _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var BurgerIngredient =
/*#__PURE__*/
function (_Component) {
  _inherits(BurgerIngredient, _Component);

  function BurgerIngredient() {
    _classCallCheck(this, BurgerIngredient);

    return _possibleConstructorReturn(this, _getPrototypeOf(BurgerIngredient).apply(this, arguments));
  }

  _createClass(BurgerIngredient, [{
    key: "render",
    value: function render() {}
  }]);

  return BurgerIngredient;
}(_react.Component);

;
var _default = burgerIngredient;
exports["default"] = _default;
//# sourceMappingURL=BurgerIngredient.js.map