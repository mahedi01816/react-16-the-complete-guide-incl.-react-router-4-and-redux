"use strict";

function _typeof23(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof23 = function _typeof23(obj) { return typeof obj; }; } else { _typeof23 = function _typeof23(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof23(obj); }

function _typeof22(obj) {
  if (typeof Symbol === "function" && _typeof23(Symbol.iterator) === "symbol") {
    _typeof22 = function _typeof22(obj) {
      return _typeof23(obj);
    };
  } else {
    _typeof22 = function _typeof22(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof23(obj);
    };
  }

  return _typeof22(obj);
}

function _typeof21(obj) {
  if (typeof Symbol === "function" && _typeof22(Symbol.iterator) === "symbol") {
    _typeof21 = function _typeof21(obj) {
      return _typeof22(obj);
    };
  } else {
    _typeof21 = function _typeof21(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof22(obj);
    };
  }

  return _typeof21(obj);
}

function _typeof20(obj) {
  if (typeof Symbol === "function" && _typeof21(Symbol.iterator) === "symbol") {
    _typeof20 = function _typeof20(obj) {
      return _typeof21(obj);
    };
  } else {
    _typeof20 = function _typeof20(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof21(obj);
    };
  }

  return _typeof20(obj);
}

function _typeof19(obj) {
  if (typeof Symbol === "function" && _typeof20(Symbol.iterator) === "symbol") {
    _typeof19 = function _typeof19(obj) {
      return _typeof20(obj);
    };
  } else {
    _typeof19 = function _typeof19(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof20(obj);
    };
  }

  return _typeof19(obj);
}

function _typeof18(obj) {
  if (typeof Symbol === "function" && _typeof19(Symbol.iterator) === "symbol") {
    _typeof18 = function _typeof18(obj) {
      return _typeof19(obj);
    };
  } else {
    _typeof18 = function _typeof18(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof19(obj);
    };
  }

  return _typeof18(obj);
}

function _typeof17(obj) {
  if (typeof Symbol === "function" && _typeof18(Symbol.iterator) === "symbol") {
    _typeof17 = function _typeof17(obj) {
      return _typeof18(obj);
    };
  } else {
    _typeof17 = function _typeof17(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof18(obj);
    };
  }

  return _typeof17(obj);
}

function _typeof16(obj) {
  if (typeof Symbol === "function" && _typeof17(Symbol.iterator) === "symbol") {
    _typeof16 = function _typeof16(obj) {
      return _typeof17(obj);
    };
  } else {
    _typeof16 = function _typeof16(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof17(obj);
    };
  }

  return _typeof16(obj);
}

function _typeof15(obj) {
  if (typeof Symbol === "function" && _typeof16(Symbol.iterator) === "symbol") {
    _typeof15 = function _typeof15(obj) {
      return _typeof16(obj);
    };
  } else {
    _typeof15 = function _typeof15(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof16(obj);
    };
  }

  return _typeof15(obj);
}

function _typeof14(obj) {
  if (typeof Symbol === "function" && _typeof15(Symbol.iterator) === "symbol") {
    _typeof14 = function _typeof14(obj) {
      return _typeof15(obj);
    };
  } else {
    _typeof14 = function _typeof14(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof15(obj);
    };
  }

  return _typeof14(obj);
}

function _typeof13(obj) {
  if (typeof Symbol === "function" && _typeof14(Symbol.iterator) === "symbol") {
    _typeof13 = function _typeof13(obj) {
      return _typeof14(obj);
    };
  } else {
    _typeof13 = function _typeof13(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof14(obj);
    };
  }

  return _typeof13(obj);
}

function _typeof12(obj) {
  if (typeof Symbol === "function" && _typeof13(Symbol.iterator) === "symbol") {
    _typeof12 = function _typeof12(obj) {
      return _typeof13(obj);
    };
  } else {
    _typeof12 = function _typeof12(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof13(obj);
    };
  }

  return _typeof12(obj);
}

function _typeof11(obj) {
  if (typeof Symbol === "function" && _typeof12(Symbol.iterator) === "symbol") {
    _typeof11 = function _typeof11(obj) {
      return _typeof12(obj);
    };
  } else {
    _typeof11 = function _typeof11(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof12(obj);
    };
  }

  return _typeof11(obj);
}

function _typeof10(obj) {
  if (typeof Symbol === "function" && _typeof11(Symbol.iterator) === "symbol") {
    _typeof10 = function _typeof10(obj) {
      return _typeof11(obj);
    };
  } else {
    _typeof10 = function _typeof10(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof11(obj);
    };
  }

  return _typeof10(obj);
}

function _typeof9(obj) {
  if (typeof Symbol === "function" && _typeof10(Symbol.iterator) === "symbol") {
    _typeof9 = function _typeof9(obj) {
      return _typeof10(obj);
    };
  } else {
    _typeof9 = function _typeof9(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof10(obj);
    };
  }

  return _typeof9(obj);
}

function _typeof8(obj) {
  if (typeof Symbol === "function" && _typeof9(Symbol.iterator) === "symbol") {
    _typeof8 = function _typeof8(obj) {
      return _typeof9(obj);
    };
  } else {
    _typeof8 = function _typeof8(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof9(obj);
    };
  }

  return _typeof8(obj);
}

function _typeof7(obj) {
  if (typeof Symbol === "function" && _typeof8(Symbol.iterator) === "symbol") {
    _typeof7 = function _typeof7(obj) {
      return _typeof8(obj);
    };
  } else {
    _typeof7 = function _typeof7(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof8(obj);
    };
  }

  return _typeof7(obj);
}

function _typeof6(obj) {
  if (typeof Symbol === "function" && _typeof7(Symbol.iterator) === "symbol") {
    _typeof6 = function _typeof6(obj) {
      return _typeof7(obj);
    };
  } else {
    _typeof6 = function _typeof6(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof7(obj);
    };
  }

  return _typeof6(obj);
}

function _typeof5(obj) {
  if (typeof Symbol === "function" && _typeof6(Symbol.iterator) === "symbol") {
    _typeof5 = function _typeof5(obj) {
      return _typeof6(obj);
    };
  } else {
    _typeof5 = function _typeof5(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof6(obj);
    };
  }

  return _typeof5(obj);
}

function _typeof4(obj) {
  if (typeof Symbol === "function" && _typeof5(Symbol.iterator) === "symbol") {
    _typeof4 = function _typeof4(obj) {
      return _typeof5(obj);
    };
  } else {
    _typeof4 = function _typeof4(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof5(obj);
    };
  }

  return _typeof4(obj);
}

function _typeof3(obj) {
  if (typeof Symbol === "function" && _typeof4(Symbol.iterator) === "symbol") {
    _typeof3 = function _typeof3(obj) {
      return _typeof4(obj);
    };
  } else {
    _typeof3 = function _typeof3(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof4(obj);
    };
  }

  return _typeof3(obj);
}

function _typeof2(obj) {
  if (typeof Symbol === "function" && _typeof3(Symbol.iterator) === "symbol") {
    _typeof2 = function _typeof2(obj) {
      return _typeof3(obj);
    };
  } else {
    _typeof2 = function _typeof2(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof3(obj);
    };
  }

  return _typeof2(obj);
}

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

function _typeof(obj) {
  if (typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol") {
    _typeof = function _typeof(obj) {
      return _typeof2(obj);
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
    };
  }

  return _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var App =
/*#__PURE__*/
function (_Component) {
  _inherits(App, _Component);

  function App() {
    _classCallCheck(this, App);

    return _possibleConstructorReturn(this, _getPrototypeOf(App).apply(this, arguments));
  }

  _createClass(App, [{
    key: "render",
    value: function render() {}
  }]);

  return App;
}(_react.Component);

var _default = App;
exports["default"] = _default;
//# sourceMappingURL=App.js.map