"use strict";

var _react = _interopRequireDefault(require("react"));

require("./Backdrop.css");

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

var backdrop = function backdrop(props) {
  return props.show;
};
//# sourceMappingURL=Backdrop.js.map