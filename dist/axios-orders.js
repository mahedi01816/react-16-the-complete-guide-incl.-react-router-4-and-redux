"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _axios = _interopRequireDefault(require("axios"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

var instance = _axios["default"].create({
  baseURL: 'https://react-my-burger-01816.firebaseio.com/'
});

var _default = instance;
exports["default"] = _default;
//# sourceMappingURL=axios-orders.js.map