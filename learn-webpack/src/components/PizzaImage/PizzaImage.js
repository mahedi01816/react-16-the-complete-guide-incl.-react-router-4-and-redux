import React from 'react';

import './PizzaImage.css';
import PizzaImage from '../../assets/nature.jpg';

const pizzaImage = (props) => (
    <div className="PizaImage">
        <img src={PizzaImage} className="PizzaImg"/>
    </div>
);

export default pizzaImage;