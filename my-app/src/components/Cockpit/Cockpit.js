import React, {useEffect, useRef, useContext} from "react";
import './Cockpit.css'
//import Radium from "radium";
import AuthContext from '../../context/auth-context';

//function cockpit(props) {


const cockpit = props => {

    const toggleBtnRef = useRef(null);
    //toggleBtnRef.current.click();
    const authContext = useContext(AuthContext);

    console.log(authContext.authenticated);

    useEffect(() => {
        console.log('[Cockpit.js] useEffect');
        /**const timer = setTimeout(() => {
            alert('Saved data to cloud');
        }, 1000);**/
        toggleBtnRef.current.click();
        return () => {
            //clearTimeout(timer);
            console.log('[Cockpit.js] cleanup work in useEffect');
        };
    }, []);

    useEffect(() => {
        console.log('[Cockpit.js] 2nd useEffect');
        return () => {
            console.log('[Cockpit.js] cleanup work in 2nd useEffect');
        };
    });

    const classes = [];
    const style = {
        backgroundColor: 'green',
        color: 'white',
        font: 'inherit',
        border: '1px solid blue',
        padding: '8px',
        cursor: 'pointer',
        ':hover': {
            backgroundColor: 'lightgreen',
            color: 'black'
        }
    };

    if (props.showPersons) {
        style.backgroundColor = 'red';
        style[':hover'] = {
            backgroundColor: 'salmon',
            color: 'black'
        };
    }

    if (props.personsLength <= 2) {
        classes.push('red');
    }

    if (props.personsLength <= 1) {
        classes.push('bold');
    }

    return (
        <div className="Cockpit">
            <h1>{props.title}</h1>
            <p className={classes.join(' ')}>This is really working!</p>
            <button
                ref={toggleBtnRef}
                style={style}
                onClick={props.clicked}>Toggle Persons
            </button>
            <button onClick={authContext.login}>Log in</button>

        </div>
    );
};

export default React.memo(cockpit);