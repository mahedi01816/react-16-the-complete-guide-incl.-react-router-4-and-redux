const numbers = [1, 2, 3];
const doubleNumberArray = numbers.map((number) => {
    return number * 2;
});

console.log(numbers);
console.log(doubleNumberArray);