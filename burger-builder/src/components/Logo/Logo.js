import React from "react";

import burgerLogo from '../../assets/images/burger_logo.jpg';
import './Logo.css';

const logo = (props) => (
    <div className="Logo" style={{height: props.height}}>
        <img src={burgerLogo} alt="Burger logo"/>
    </div>
);

export default logo;