import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://react-my-burger-01816.firebaseio.com/'
});

export default instance;